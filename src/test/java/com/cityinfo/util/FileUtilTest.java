package com.cityinfo.util;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FileUtilTest{

	@Before
	public void setup(){
		
	}
	
	@Test
	public void shouldReturnTrueForAValidFileExtensionType(){
		boolean actual = FileUtil.isValidFileExtensionTypeOfDAT("cityinfo.dat");

		assertTrue(actual);
	}
	
	@Test
	public void shouldReturnFalseForMissingFileExtensionType(){
		boolean actual = FileUtil.isValidFileExtensionTypeOfDAT("cityinfo");
		
		assertFalse(actual);
	}
	
	@Test
	public void shouldReturnFalseForAnInvalidFileExtensionType(){
		boolean actual = FileUtil.isValidFileExtensionTypeOfDAT("cityinfo.exe");
		
		assertFalse(actual);
	}
	
	@Test
	public void shouldReturnFalseForAMissingFilenameWithAnExtensionType(){
		boolean actual = FileUtil.isValidFileExtensionTypeOfDAT(".dat");
		
		assertFalse(actual);
	}
	
	@After
	public void shutdown(){
		
	}
}