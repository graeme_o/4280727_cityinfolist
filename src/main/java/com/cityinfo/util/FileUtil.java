package com.cityinfo.util;

public class FileUtil {

	/**
	 *  Validates file extension type of a given filename.
	 * @param filename
	 * @return boolean
	 */
	public static boolean isValidFileExtensionTypeOfDAT(String filename) {
		return String.valueOf(filename).endsWith(".dat") && !String.valueOf(filename).startsWith(".");
	}
	
}
