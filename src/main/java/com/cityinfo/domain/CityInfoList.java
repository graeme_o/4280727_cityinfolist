package com.cityinfo.domain;

import java.util.ArrayList;

public class CityInfoList {

	private ArrayList<CityInfo> cityInfos;

	public CityInfoList(ArrayList<CityInfo> cityInfos) {
		this.cityInfos = cityInfos;
	}

	private double avgPopulation() {
		int totalPopulation = 0;

		for (CityInfo cityInfo : cityInfos) {
			totalPopulation = totalPopulation + cityInfo.getCurrentPopulation();
		}

		return totalPopulation / cityInfos.size();
	}

	private double avgArea() {
		double totalArea = 0;

		for (CityInfo cityInfo : cityInfos) {
			totalArea = totalArea + cityInfo.getArea();
		}

		return totalArea / cityInfos.size();
	}

	private double avgDensity() {
		double totalDensity = 0;

		for (CityInfo cityInfo : cityInfos) {
			totalDensity = totalDensity + cityInfo.calculateDensity();
		}

		return totalDensity / cityInfos.size();
	}

	public double avgAnnualGrowth() {
		return 0;
	}

	public String largestCity() {
		return null;
	}

	public String smallestCity() {
		return null;
	}

	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();

		for (CityInfo cityInfo : cityInfos) {
			stringBuilder.append("City: " + cityInfo.getCity() + ", "
					+ cityInfo.getState() + " (Founded in "
					+ cityInfo.getFoundingYear() + ")\n");
			stringBuilder.append("Population: "
					+ cityInfo.getCurrentPopulation() + "\n");
			stringBuilder.append("Area: " + cityInfo.getArea() + " sq mi\n");
			stringBuilder.append("Density: " + cityInfo.calculateDensity()
					+ " people per sq mi\n");
			stringBuilder.append("Growth: " + cityInfo.calculateGrowth()
					+ " people per yr\n");
		}

		stringBuilder.append("\n");
		stringBuilder.append("---------- Summary for List ---------\n");
		stringBuilder.append("Average Population :" + avgPopulation() + "\n");

		return stringBuilder.toString();
	}
}
