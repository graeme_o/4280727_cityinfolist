package com.cityinfo.domain;

public class CityInfo {

	private String city;
	private String state;
	private int foundingYear;
	private int initialPopulation;
	private int currentPopulation;
	private double area;
	
	public static final int CENSUS_YEAR = 2010;
	
	public CityInfo(String city, String state, int foundingYear, int initialPopulation, int currentPopulation, double area) {
		this.city = city;
		this.state = state;
		this.foundingYear = foundingYear;
		this.initialPopulation = initialPopulation;
		this.currentPopulation = currentPopulation;
		this.area = area;
		
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getFoundingYear() {
		return foundingYear;
	}

	public void setFoundingYear(int foundingYear) {
		this.foundingYear = foundingYear;
	}

	public int getInitialPopulation() {
		return initialPopulation;
	}

	public void setInitialPopulation(int initialPopulation) {
		this.initialPopulation = initialPopulation;
	}

	public int getCurrentPopulation() {
		return currentPopulation;
	}

	public void setCurrentPopulation(int currentPopulation) {
		this.currentPopulation = currentPopulation;
	}

	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
	}

	public double calculateDensity(){
		return currentPopulation / area;
	}
	
	public double calculateGrowth(){
		return 0;
	}
}
